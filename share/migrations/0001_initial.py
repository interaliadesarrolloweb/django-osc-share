# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SharePlugin'
        db.create_table('cmsplugin_shareplugin', (
            ('cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('share_text', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('share_on_twitter', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('twitter_account', self.gf('django.db.models.fields.CharField')(default='', max_length=50, null=True, blank=True)),
            ('widget_type', self.gf('django.db.models.fields.CharField')(default='xfbml', max_length=10)),
            ('like_on_facebook', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('like_with_send', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('like_with_faces', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('like_layout', self.gf('django.db.models.fields.CharField')(default='button_counter', max_length=20)),
            ('like_colorscheme', self.gf('django.db.models.fields.CharField')(default='light', max_length=30)),
            ('like_font', self.gf('django.db.models.fields.CharField')(default='lucida grande', max_length=30)),
            ('like_width', self.gf('django.db.models.fields.CharField')(default='450', max_length=10)),
            ('like_height', self.gf('django.db.models.fields.CharField')(default='21', max_length=10)),
            ('share_on_facebook', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('gplus_button', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('gplus_size', self.gf('django.db.models.fields.CharField')(default='standard', max_length=20)),
            ('gplus_counter', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('share_on_gplus', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('share', ['SharePlugin'])


    def backwards(self, orm):
        # Deleting model 'SharePlugin'
        db.delete_table('cmsplugin_shareplugin')


    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 12, 26, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        'share.shareplugin': {
            'Meta': {'object_name': 'SharePlugin', 'db_table': "'cmsplugin_shareplugin'", '_ormbases': ['cms.CMSPlugin']},
            'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'gplus_button': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'gplus_counter': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'gplus_size': ('django.db.models.fields.CharField', [], {'default': "'standard'", 'max_length': '20'}),
            'like_colorscheme': ('django.db.models.fields.CharField', [], {'default': "'light'", 'max_length': '30'}),
            'like_font': ('django.db.models.fields.CharField', [], {'default': "'lucida grande'", 'max_length': '30'}),
            'like_height': ('django.db.models.fields.CharField', [], {'default': "'21'", 'max_length': '10'}),
            'like_layout': ('django.db.models.fields.CharField', [], {'default': "'button_counter'", 'max_length': '20'}),
            'like_on_facebook': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'like_width': ('django.db.models.fields.CharField', [], {'default': "'450'", 'max_length': '10'}),
            'like_with_faces': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'like_with_send': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_on_facebook': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_on_gplus': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'share_on_twitter': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'share_text': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'twitter_account': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'widget_type': ('django.db.models.fields.CharField', [], {'default': "'xfbml'", 'max_length': '10'})
        }
    }

    complete_apps = ['share']