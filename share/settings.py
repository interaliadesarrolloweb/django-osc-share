# coding: utf-8
__author__ = 'luis.villanueva'

from django.conf import settings

SHARE_EMAIL_FROM = getattr(settings, 'SHARE_EMAIL_FROM', settings.EMAIL_FROM)
SHARE_EMAIL_FORM_CACHE = getattr(settings, 'SHARE_EMAIL_FORM_CACHE', 48*60*60)
SHARE_EMAIL_DISPLAY_NAME = getattr(settings, 'SHARE_EMAIL_DISPLAY_NAME', 'Django Website')
