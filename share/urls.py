# coding: utf-8
__author__ = 'luis.villanueva'
from django.conf.urls.defaults import *

urlpatterns = patterns(
    'share.views',
    url(r"^send_email/$", "send_to_a_friend", name="send_to_a_friend"),
)
