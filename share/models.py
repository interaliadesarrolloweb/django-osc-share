from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
__author__ = 'luis.villanueva'


FACEBOOK_LIKE_LAYOUTS = (
    ('standard', _("Standard")),
    ('button_count', _("Button with counter")),
    ('box_count', _("Box with counter")),
)

FACEBOOK_LIKE_COLORSCHEMES = (
    ('light', _('light')),
    ('dark', _('dark')),
)

FACEBOOK_LIKE_FONTS = (
    ('arial', 'arial'),
    ('lucida grande', 'lucida grande'),
    ('segoe ui', 'segoe ui'),
    ('tahoma', 'tahoma'),
    ('trebuchet ms', 'trebuchet ms'),
    ('verdana', 'verdana'),
)

GPLUS_SIZES = (
    ('small', _("Small")),
    ('medium', _("Medium")),
    ('standard', _("Standard")),
    ('tall', _("Tall")),
)


class SharePlugin(CMSPlugin):
    url = models.URLField(
        null=True, blank=True, help_text=_(u"Will use page's url if blank."))
    share_text = models.CharField(
        blank=True, null=True, max_length=100,
        help_text=_(u"Will use page's title if blank."))
    # Twitter
    share_on_twitter = models.BooleanField(default=True)
    twitter_account = models.CharField(
        max_length=50, blank=True, null=True,
        default=getattr(settings, 'TWITTER_ACCOUNT', ''))
    # Facebook
    widget_type = models.CharField(
        max_length=10, choices=(('xfbml', "XFBML"), ("iframe", "iframe")),
        default="xfbml")
    like_on_facebook = models.BooleanField(default=True)
    like_with_send = models.BooleanField(default=False)
    like_with_faces = models.BooleanField(default=False)
    like_layout = models.CharField(
        max_length=20, default="button_counter", choices=FACEBOOK_LIKE_LAYOUTS)
    like_colorscheme = models.CharField(
        max_length=30, default="light", choices=FACEBOOK_LIKE_COLORSCHEMES)
    like_font = models.CharField(
        max_length=30, default='lucida grande', choices=FACEBOOK_LIKE_FONTS)
    like_width = models.CharField(max_length=10, default="450")
    like_height = models.CharField(max_length=10, default="21")
    share_on_facebook = models.BooleanField(default=False)
    # Google+
    gplus_button = models.BooleanField(_("show +1 button"), default=True)
    gplus_size = models.CharField(
        _("+1 button size"), max_length=20, choices=GPLUS_SIZES,
        default="standard")
    gplus_counter = models.BooleanField(
        _("+1 button with counter"), default=False)
    share_on_gplus = models.BooleanField(
        default=False,
        help_text=_("This feature is experimental and not recommended."))
