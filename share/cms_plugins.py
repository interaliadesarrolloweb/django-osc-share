# coding: utf-8
__author__ = 'jony'
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import SharePlugin


class CMSSharePlugin(CMSPluginBase):
    """
    Clase plugin para agregar formularios
    """
    model = SharePlugin
    name = _(u"Share Buttons")
    render_template = "share/share_render.html"

    def render(self, context, instance, placeholder, request=None):
        if request is None:
            request = context.get('request')
        url = instance.url
        if not url:
            url_parts = (
                request.is_secure() and "https" or "http",
                request.META.get("HTTP_HOST"),
                request.META.get("PATH_INFO"),
                "?%s" % request.META.get("QUERY_STRING") if request.META.get(
                    "QUERY_STRING") else "",
            )
            url = "%s://%s%s%s" % url_parts
        text = instance.share_text
        if not text:
            text = unicode(context.get('current_page'))
        twitter_account = instance.twitter_account
        if not twitter_account:
            twitter_account = settings.TWITTER_ACCOUNT
        context = {
            'instance': instance,
            'url': url,
            'share_text': text,
            'twitter_account': twitter_account,
            'type': instance.widget_type,

        }

        return context

plugin_pool.register_plugin(CMSSharePlugin)
