# coding: utf-8
from django.contrib.sites.models import Site
from django.core.mail.message import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from settings import SHARE_EMAIL_FROM, SHARE_EMAIL_DISPLAY_NAME
from htmlentitydefs import name2codepoint as n2cp
import re

__author__ = 'luis.villanueva'

from django import forms
import logging
log = logging.getLogger("custom")


def substitute_entity(match):
    ent = match.group(2)
    if match.group(1) == "#":
        return unichr(int(ent))
    else:
        cp = n2cp.get(ent)

        if cp:
            return unichr(cp)
        else:
            return match.group()


def decode_htmlentities(string):
    entity_re = re.compile("&(#?)(\d{1,5}|\w{1,8});")
    return entity_re.subn(substitute_entity, string)[0]


class SendToAFriendForm(forms.Form):
    full_name = forms.CharField(label=_("Your full name"), max_length=100)
    from_email = forms.EmailField(label=_("Tu correo electronico."))
    to_email_1 = forms.EmailField(label=_("Send to"), required = False)
    to_email_2 = forms.EmailField(required=False)
    to_email_3 = forms.EmailField(required=False)
    title = forms.CharField(max_length=200, widget=forms.HiddenInput)
    link = forms.URLField(widget=forms.HiddenInput)
    subject = forms.CharField(max_length=100, widget=forms.HiddenInput)

    def get_recipient_list(self):
        to_email_1 = self.cleaned_data.get('to_email_1')
        to_email_2 = self.cleaned_data.get('to_email_2')
        to_email_3 = self.cleaned_data.get('to_email_3')
        recipient_list = []
        if to_email_1:
            recipient_list.append(to_email_1)
        if to_email_2:
            recipient_list.append(to_email_2)
        if to_email_3:
            recipient_list.append(to_email_2)
        return recipient_list

    def send(self, display_name=None, subject=None):
        full_name = self.cleaned_data.get('full_name')
        from_email = self.cleaned_data.get('from_email')
        recipient_list = self.get_recipient_list()
        title = self.cleaned_data.get("title")
        if display_name is None:
            display_name = SHARE_EMAIL_DISPLAY_NAME
        import HTMLParser
        h = HTMLParser.HTMLParser()
        title = h.unescape(title)
        if not subject:
            subject = _(u"%(sender)s te envia: %(title)s") % {
                'sender': full_name,
                'title': title,
            }
        else:
            subject = u"%s %s" % (full_name, subject)
        title_ = decode_htmlentities(self.cleaned_data.get("title"))
        self.cleaned_data.update({
            'SITE': Site.objects.get_current(),
            'title': title_
        })
        message = render_to_string(
            "share/email/send_to_a_friend.html",
            self.cleaned_data
        )
        from_ = u"'%s' <%s>" % (display_name, SHARE_EMAIL_FROM)
        log.info("test %s" % from_)

        email = EmailMessage(
            subject,
            message,
            from_,
            [],
            recipient_list,
            headers={'Reply-To': from_email}
        )
        email.content_subtype = 'html'
        email.send(fail_silently=False)
        return True

    def clean(self):
        to_email_1 = self.cleaned_data.get('to_email_1',None)
        to_email_2 = self.cleaned_data.get('to_email_2',None)
        to_email_3 = self.cleaned_data.get('to_email_3',None)
        if not to_email_1 and not to_email_2 and not to_email_3:
            raise forms.ValidationError(_("Ingresa almenos un email"))
        return self.cleaned_data

