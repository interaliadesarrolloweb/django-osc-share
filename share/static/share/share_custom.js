// Share functions
function openShareWindow(url, width, height){
    if(width==undefined) width="400";
    if(height==undefined) height="400";
    window.open(url, "", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width="+width+",height="+height+',left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');
}

function msieversion()
{
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf ( "MSIE " );

  if ( msie > 0 )      // If Internet Explorer, return version number
     return parseInt (ua.substring (msie+5, ua.indexOf (".", msie )));
  else                 // If another browser, return 0
     return 0;

}

function gplus_share(url, text){
    var content = text+"+-+"+url;
    window.open('https://m.google.com/app/plus/x/?v=compose&content='+content,
        'gplusshare','width=450,height=300,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');
}
(function($){
    head.ready(function(){
       $("a.email_share_button").colorbox({iframe:true, width:300, height:400});
    });
}(jQuery));