# coding: utf-8
__author__ = 'luis.villanueva'

import hashlib
import simplejson
from django.shortcuts import render
from django.http import HttpResponse
from django.core.cache import cache
from .forms import SendToAFriendForm
from .settings import SHARE_EMAIL_FORM_CACHE
from django.utils.translation import ugettext_lazy as _
try:
    from django.views.decorators.csrf import ensure_csrf_cookie
except ImportError:
    # Django 1.3.x issue https://code.djangoproject.com/ticket/15354
    from django.middleware.csrf import CsrfViewMiddleware, get_token
    from django.utils.decorators import decorator_from_middleware

    class _EnsureCsrfCookie(CsrfViewMiddleware):
        def _reject(self, request, reason):
            return None

        def process_view(
                self, request, callback, callback_args, callback_kwargs):
            retval = super(_EnsureCsrfCookie, self).process_view(
                request, callback, callback_args, callback_kwargs)
            # Forces process_response to send the cookie
            get_token(request)
            return retval

    ensure_csrf_cookie = decorator_from_middleware(_EnsureCsrfCookie)
    ensure_csrf_cookie.__name__ = 'ensure_csrf_cookie'
    ensure_csrf_cookie.__doc__ = """
    Use this decorator to ensure that a view sets a CSRF cookie, whether or
    not it uses the csrf_token template tag, or the CsrfViewMiddleware is used.
    """


def form_errors(form):
    errores = {}
    for field, error in form._errors.iteritems():
        errs = []
        for e in error:
            errs.append(unicode(e))
        errores[field] = errs
    return errores


def serialize_errors(forms, posted):
    if not(type(forms) == list or type(forms) == tuple):
        forms = [forms]
    errores = {}
    for form in forms:
        errores.update(form_errors(form))
    response = {
        'values': posted,
        'errors': errores
    }

    json = simplejson.dumps(response)
    return json


@ensure_csrf_cookie
def send_to_a_friend(request):
    subject = request.REQUEST.get("subject", "")
    if request.method == "POST" and request.is_ajax():
        form = SendToAFriendForm(request.POST)
        if form.is_valid():
            if subject == "":
                success = form.send()
            else:
                success = form.send(subject=subject)
            result = {
                'success': True,
                'errors': [],
            }
            result = simplejson.dumps(result)
        else:
            result = serialize_errors(form, request.POST)
        return HttpResponse(result, mimetype="application/json")
    link = request.GET.get('link',"")
    title = request.GET.get('title',"")
    mobile = request.REQUEST.get("mobile","")

    initial = {
        'link': link,
        'title': title,
        'subject': subject,
    }

    form = SendToAFriendForm(initial=initial)
    context = {
        'form': form,
        'link': link,
        'title': title,
        'subject': subject,
        'is_mobile': 'true' if mobile == u'1' else 'false',
    }

    response = render(request, 'share/send_to_a_friend_form.html', context)
    return response
