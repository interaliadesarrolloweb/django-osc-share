# coding: utf-8
__author__ = 'luis.villanueva'

from urllib import urlencode
from django import template
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import get_language
register = template.Library()


@register.inclusion_tag("share/twitter_share.html")
def twitter_share(
        url=None, text=None, count='horizontal', twitter_account=None):
    if twitter_account is None:
        try:
            twitter_account = settings.TWITTER_ACCOUNT
        except AttributeError:
            raise ImproperlyConfigured(
                "No TWITTER_ACCOUNT setting was found.")
    context = {
        'url': url,
        'text': text,
        'count': count,
        'lang': get_language(),
        'twitter_account': twitter_account,
    }
    return context


@register.inclusion_tag("share/twitter_share_light.html")
def twitter_share_light(url, text, twitter_account=None):
    if twitter_account is None:
        try:
            twitter_account = settings.TWITTER_ACCOUNT
        except AttributeError:
            raise ImproperlyConfigured(
                "No TWITTER_ACCOUNT setting was found.")

    context = {
        'url': urlencode({'u': url})[2:],
        #'text':urlencode({'u':text})[2:],
        'text': text,
        'lang': get_language(),
        'twitter_account': twitter_account,
        'MEDIA_URL': settings.MEDIA_URL,
    }
    return context


@register.inclusion_tag("share/facebook_like.html")
def facebook_like(
        url=None, send=False, faces=False, layout="button_count",
        colorscheme="light", font='lucida grande', width_=80, height=""):

    if not layout:
        layout = "button_count"
    if not colorscheme:
        colorscheme = "light"
    if not font:
        font = "lucida grande"
    if not width_:
        width_ = 80

    context = {
        'FACEBOOK_APP_ID': settings.FACEBOOK_APP_ID,
        'url': url,
        'send': send,
        'faces': faces,
        'layout': layout,
        'colorscheme': colorscheme,
        'font': font,
        'width': width_,
        'lang': get_language(),
    }
    return context


@register.inclusion_tag("share/facebook_share.html")
def facebook_share(url, text):

    context = {
        'FACEBOOK_APP_ID': settings.FACEBOOK_APP_ID,
        'url': urlencode({'u': url})[2:],
        'text': text,
        'lang': get_language(),
        'MEDIA_URL': settings.MEDIA_URL,
    }
    return context


@register.inclusion_tag("share/gplus_share.html")
def gplus_share(url, text):

    context = {
        'url': urlencode({'u': url})[2:],
        'text': text,
        'lang': get_language(),
        'STATIC_URL': settings.STATIC_URL,
    }
    return context


@register.inclusion_tag("share/gplus_button.html")
def gplus_button(url=None, size="standard", count="false", callback=None):

    context = {
        'url': url,
        'size': size,
        'count': count,
        'callback': callback,
    }
    return context


@register.inclusion_tag("share/send_to_a_friend_button.html")
def email_friend(link, title, subject):
    import cgi
    title = cgi.escape(title).encode('ascii', 'xmlcharrefreplace')

    params = {
        'link': link,
        'title': title,
        'subject': subject,
    }

    context = {
        'MEDIA_URL': settings.MEDIA_URL,
    }
    context.update(params)
    context['params'] = urlencode(params)
    return context
