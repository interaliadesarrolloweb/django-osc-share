# coding: utf-8
from distutils.core import setup
import os

# Snippet taken from django-registration setup
# Compile the list of packages available, because distutils doesn't have
# an easy way to do this.
packages, data_files = [], []
root_dir = os.path.dirname(__file__)
if root_dir:
    os.chdir(root_dir)

for dirpath, dirnames, filenames in os.walk('share'):
    # Ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'):
            del dirnames[i]
    if '__init__.py' in filenames:
        pkg = dirpath.replace(os.path.sep, '.')
        if os.path.altsep:
            pkg = pkg.replace(os.path.altsep, '.')
        packages.append(pkg)
    elif filenames:
        prefix = dirpath[6:]  # Strip "share/" or "share\"
        for f in filenames:
            data_files.append(os.path.join(prefix, f))
# end of snippet
setup(
    name='osc_share',
    version='0.1',
    description='OSC Share',
    author='Open Source Interalia.net',
    author_email='desarrolloweb@interalia.net',
    url='https://bitbucket.org/interaliadesarrolloweb/django-osc-share',
    packages=packages,
    package_data={'share': data_files}
)
